MODULE ReportsModule
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !
    !  This module generates system status for reporting to MES
    !  cycle time is generated and the last 45 times are saved in the array LastCycleTime
    !  Paused or running is set in Run_Paused where 1 is running and 2 is paused
    !  Picking or placing is set in bPicking and bPlacing. Each is set TRUE when doing that task
    !  There are two virtual outputs that are set for the conditions Starved and Blocked. They are do_Starved and do_Blocked
    !  Speed % is set in speedOvrd. It will be a number from 0 to 100
    !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    VAR clock CycleTimeclk;
    PERS num CurrentcycleTime;
    PERS num LastCycleTime{45}:=[12.533,5.312,20.145,1503.03,189.995,16.604,19.39,19.564,15.433,2514.05,19.616,18.575,90.431,19.391,19.624,18.036,286.162,20.169,19.633,19.382,19.519,245.487,19.624,19.362,19.604,19.419,361.603,15.391,19.018,19.003,18.991,16.725,140.256,19.023,18.846,79.006,13.729,19.427,19.499,72.134,51.957,19.386,19.616,15.39,117.011];
    VAR bool bCurrentState:=FALSE;
    VAR intnum IntVacOff;
    VAR intnum IntVacOn;
    PERS num SpeedOvrd:=100;
    PERS num Auto_Man:=1;
    !Auto = 1 Manual = 2
    PERS num Run_Paused:=2;
    !running = 1 Paused = 2
    PERS bool bPlacing:=TRUE;
    PERS bool bPicking:=FALSE;

    PROC main()
        InitInts;
        WHILE TRUE DO
            ClkStart CycleTimeclk;
            SpeedOvrd:=CSpeedOverride();
            IF DOutput(do_InAuto)=1 THEN
                Auto_Man:=1;
            ELSE
                Auto_Man:=2;
            ENDIF
            IF DOutput(do_TaskExec)=1 THEN
                Run_Paused:=1;
            ELSE
                Run_Paused:=2;
            ENDIF
            WaitTime 0.05;
        ENDWHILE

    ENDPROC

    PROC InitInts()
        CONNECT IntVacOff WITH TimeTrap;
        ISignalDO do_VacON,0,IntVacOff;
        CONNECT IntVacOn WITH PlaceTrap;
        ISignalDO do_VacON,1,IntVacOn;
    ENDPROC

    TRAP TimeTrap
        bPicking:=TRUE;
        bPlacing:=FALSE;
        FOR i FROM 45 TO 2 STEP -1 DO
            LastCycleTime{i}:=LastCycleTime{i-1};
        ENDFOR
        ClkStop CycleTimeclk;
        LastCycleTime{1}:=CurrentcycleTime;
        CurrentcycleTime:=ClkRead(CycleTimeclk);
        ClkReset CycleTimeclk;
        ClkStart CycleTimeclk;
    ENDTRAP

    TRAP PlaceTrap
        bPicking:=FALSE;
        bPlacing:=TRUE;
    ENDTRAP
ENDMODULE