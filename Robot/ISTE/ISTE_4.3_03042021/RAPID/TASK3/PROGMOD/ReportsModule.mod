MODULE ReportsModule
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !
    !  This module generates system status for reporting to MES
    !  cycle time is generated and the last 45 times are saved in the array LastCycleTime
    !  Paused or running is set in Run_Paused where 1 is running and 2 is paused
    !  Picking or placing is set in bPicking and bPlacing. Each is set TRUE when doing that task
    !  There are two virtual outputs that are set for the conditions Starved and Blocked. They are do_Starved and do_Blocked
    !  Speed % is set in speedOvrd. It will be a number from 0 to 100
    !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    VAR clock CycleTimeclk;
    PERS num CurrentcycleTime;
    PERS num LastCycleTime{45}:=[22.2,22.575,22.225,22.201,22.604,22.188,22.197,22.604,22.208,22.201,22.58,22.22,20.748,24.037,22.209,22.208,22.705,22.221,51.074,21.995,22.874,22.136,21.975,21.023,21.89,21.108,21.979,14.753,22.838,22.22,22.208,22.614,22.181,22.2,22.669,22.213,22.132,61.545,22.213,22.225,22.608,22.209,22.007,22.769,22.237];
    VAR bool bCurrentState:=FALSE;
    VAR intnum IntVacOff;
    VAR intnum IntVacOn;
    PERS num SpeedOvrd:=100;
    PERS num Auto_Man:=1;
    !Auto = 1 Manual = 2
    PERS num Run_Paused:=2;
    !running = 1 Paused = 2
    PERS bool bPlacing:=FALSE;
    PERS bool bPicking:=TRUE;

    PROC main()
        InitInts;
        WHILE TRUE DO
            ClkStart CycleTimeclk;
            SpeedOvrd:=CSpeedOverride();
            IF DOutput(do_InAuto)=1 THEN
                Auto_Man:=1;
            ELSE
                Auto_Man:=2;
            ENDIF
            IF DOutput(do_TaskExec)=1 THEN
                Run_Paused:=1;
            ELSE
                Run_Paused:=2;
            ENDIF
            WaitTime 0.05;
        ENDWHILE

    ENDPROC

    PROC InitInts()
        CONNECT IntVacOff WITH TimeTrap;
        ISignalDO do_VacON,0,IntVacOff;
        CONNECT IntVacOn WITH PlaceTrap;
        ISignalDO do_VacON,1,IntVacOn;
    ENDPROC

    TRAP TimeTrap
        bPicking:=TRUE;
        bPlacing:=FALSE;
        FOR i FROM 45 TO 2 STEP -1 DO
            LastCycleTime{i}:=LastCycleTime{i-1};
        ENDFOR
        ClkStop CycleTimeclk;
        LastCycleTime{1}:=CurrentcycleTime;
        CurrentcycleTime:=ClkRead(CycleTimeclk);
        ClkReset CycleTimeclk;
        ClkStart CycleTimeclk;
    ENDTRAP

    TRAP PlaceTrap
        bPicking:=FALSE;
        bPlacing:=TRUE;
    ENDTRAP
ENDMODULE