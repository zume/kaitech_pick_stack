MODULE mDataModule
	PERS tooldata tVac:=[TRUE,[[0,0,210],[1,0,0,0]],[0.3,[0,0,75],[1,0,0,0],0,0,0]];
    
!	TASK PERS wobjdata Wobj_Table:=[FALSE,TRUE,"",[[494.31,-641.459,-1694.17],[0.861809,-0.00479171,0.00162969,0.507207]],[[0,0,0],[1,0,0,0]]];
    TASK PERS wobjdata Wobj_Zero:=[FALSE,TRUE,"",[[0,0,0],[0.862029,-0.00506621,0.00168196,0.506831]],[[0,0,0],[1,0,0,0]]];
    
!    TASK PERS wobjdata Wobj_Table:=[FALSE,TRUE,"",[[493.921,-641.946,-1694.38],[0.861978,-0.00458628,0.00160928,0.506922]],[[0,0,0],[1,0,0,0]]];
    TASK PERS wobjdata Wobj_Table:=[FALSE,TRUE,"",[[456.757,-526.054,-1613.28],[0.861201,-0.00316998,0.000532212,0.508255]],[[0,0,0],[1,0,0,0]]];
    TASK PERS wobjdata Wobj_Table1 := [FALSE, TRUE, "", [[445.876, -536.28, -1579.99],[0.862767, -0.00881498, -0.00425301, 0.505507]],[[0, 0, 0],[1, 0, 0, 0]]];
    TASK PERS wobjdata Wobj_Table2 := [FALSE, TRUE, "", [[14.2725, -289.531, -1586.91],[0.863238, -9.71527E-05, -0.00474138, 0.504774]],[[0, 0, 0],[1, 0, 0, 0]]];
    TASK PERS wobjdata Wobj_Table3 := [FALSE, TRUE, "", [[-420.055, -39.5055, -1587.88],[0.864895, 0.0061686, -0.000752318, 0.501915]],[[0, 0, 0],[1, 0, 0, 0]]];
    PERS wobjdata Wobj_Table_Temp := [FALSE, TRUE, "", [[14.2725, -289.531, -1586.91],[0.863238, -9.71527E-05, -0.00474138, 0.504774]],[[0, 0, 0],[1, 0, 0, 0]]];
    
	CONST robtarget rtPick1:=[[3.05,3.15,-8.89],[0.00751993,-0.714364,0.699706,0.0062644],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPick16:=[[1.11,-0.51,-10.63],[0.00108686,0.710308,-0.703875,-0.00461615],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPick17:=[[177.64,-1.03,-8.13],[0.00108686,0.710308,-0.703875,-0.00461615],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPick18:=[[353.86,-0.78,-11.13],[0.001087,0.710287,-0.703897,-0.00461612],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPick19:=[[531.42,-0.20,-7.53],[0.001087,0.710287,-0.703897,-0.00461612],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPick20:=[[707.76,-0.42,-8.52],[0.00108713,0.710266,-0.703918,-0.00461609],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPick21:=[[3.88,146.33,-10.87],[0.00108727,0.710244,-0.703939,-0.00461606],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPick22:=[[177.70,145.41,-10.65],[0.00108727,0.710244,-0.703939,-0.00461606],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPick23:=[[356.98,145.77,-12.07],[0.00108747,0.710215,-0.703969,-0.00461601],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPick24:=[[533.02,143.02,-7.70],[0.00108741,0.710223,-0.70396,-0.00461602],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPick25:=[[707.73,144.67,-8.04],[0.00108741,0.710223,-0.70396,-0.00461602],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPick26:=[[6.51,291.31,-7.49],[0.00108741,0.710223,-0.70396,-0.00461602],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPick27:=[[181.36,292.67,-9.28],[0.00108727,0.710244,-0.703939,-0.00461606],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPick28:=[[357.94,290.96,-6.93],[0.00108705,0.710278,-0.703905,-0.00461611],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPick29:=[[534.51,289.55,-7.83],[0.00108713,0.710266,-0.703918,-0.00461609],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPick30:=[[711.45,288.74,-10.72],[0.00108719,0.710257,-0.703926,-0.00461608],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPick31:=[[3.89,0.70,-8.68],[0.0061641,0.714836,-0.699264,0.00078836],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPlace:=[[-742.49,6.90,-1441.87],[0.00532896,-0.701349,0.712798,-0.000312448],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPlace_Cam:=[[13.18,139.66,-51.21],[0.709132,0.0021737,0.0019741,0.70507],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtClearTooling:=[[-624.38,-3.10,-1328.45],[0.00532772,-0.704094,0.710086,-0.00033301],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget rtTemp;
    PERS robtarget rtTemp1;
    PERS robtarget rtPickLoc;
    PERS robtarget rtPlace_Cam_T;
    CONST robtarget rtClearPath1:=[[-577.48,343.96,-1455.20],[0.00533323,-0.690025,0.723766,-0.0002284],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtClearPath2:=[[-577.48,343.96,-1455.20],[0.00533323,-0.690025,0.723766,-0.0002284],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtClearPath3:=[[-577.48,343.96,-1455.20],[0.00533323,-0.690025,0.723766,-0.0002284],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtClearPath4:=[[-577.48,343.96,-1455.20],[0.00533323,-0.690025,0.723766,-0.0002284],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    VAR num offset_X;
    VAR num offset_y;
    VAR num offset_X_Mask;
    VAR num offset_y_Mask;
    VAR num offset_X_Jig;
    VAR num offset_y_Jig; 
    VAR num offset_X_total;
    VAR num offset_Y_total;
    VAR num XFinal_Pos;
    VAR num YFinal_Pos;
    VAR num offset_Threshold_Pos;
    VAR num offset_Threshold_Neg;
    VAR bool CameraEnabled:=false;
    VAR num offset_angle;
    PERS num TAG1:=10;
    PERS bool tag2:=true;
    PERS string tag3:="Testing";

    !Calibration Camera Routine
    CONST robtarget rtCal_1_Point:=[[13.18,137.66,-71.2],[0.709132,0.0021737,0.0019741,0.70507],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtCal_2_Point:=[[11.18,135.66,-71.2],[0.709132,0.0021737,0.0019741,0.70507],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtCal_3_Point:=[[11.18,139.66,-71.2],[0.709132,0.0021737,0.0019741,0.70507],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtCal_4_Point:=[[15.18,139.66,-71.2],[0.709132,0.0021737,0.0019741,0.70507],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtCal_5_Point:=[[15.18,135.66,-71.2],[0.709132,0.0021737,0.0019741,0.70507],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtCal_Val_Point_1:=[[12.18,136.66,-141.25],[0.709132,0.0021737,0.0019741,0.70507],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtCal_Val_Point_2:=[[14.18,138.66,-141.25],[0.709132,0.0021737,0.0019741,0.70507],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];   
    

    VAR triggdata VacON;
    VAR triggdata VacOFF;
    
    PERS speeddata v10k:=[10000,5000,1,1];
    
    PERS bool bFirstTime:=TRUE;
    PERS bool bLoadMaskReady:=FALSE;
    PERS bool bTableLoaded:=FALSE;
    PERS bool bTableClear:=FALSE;
    
    VAR num Xoffs:=0;
    VAR num Yoffs:=0;
    VAR num Zoffs:=100;
    VAR num ZPlace:=90;
    VAR num j;
    VAR num VacOnTime:=0.05;!use 0 for simulation use 0.05 to ?? for real application
    VAR num VacOffTime:=0.07;!use 0 for simulation use 0.05 to ?? for real application
    VAR num Val;
    PERS num MaskPickLoc:=16;
    VAR num nPickDwell:=0.1;
    VAR num nPlaceDwell:=0.25;
    PERS num nXoffSet:=174.24;
	TASK PERS tooldata tPointer:=[TRUE,[[0,0,300],[1,0,0,0]],[0.3,[0,0,75],[1,0,0,0],0,0,0]];
	TASK PERS wobjdata wobj_plate:=[FALSE,TRUE,"",[[-489.426,-602.569,-1487.04],[0.00158833,0.86753,0.497377,-0.00246967]],[[0,-2,0],[1,0,0,0]]];
    
    
    
    
ENDMODULE