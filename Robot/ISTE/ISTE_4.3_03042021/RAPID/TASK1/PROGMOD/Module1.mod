MODULE Module1
  
        
    !***********************************************************
    !
    ! Module:  Module1
    !
    ! Description:
    !   <Insert description here>
    !
    ! Author: steve
    !
    ! Version: 1.0
    !
    !***********************************************************
    
    
    !***********************************************************
    !
    ! Procedure main
    !
    !   This is the entry point of your program
    !
    !***********************************************************
    PROC main()
        InitTriggs;
        InitIO;
        StartUp;
        CornerPathWarning FALSE;
        WHILE TRUE DO
        WHILE MaskPickLoc <= 15 DO

            TEST MaskPickLoc
            CASE 1:
                rtPickLoc:=rtPick16;
            CASE 2:
                rtPickLoc:=rtPick17;
            CASE 3:
                rtPickLoc:=rtPick18;
            CASE 4:
                rtPickLoc:=rtPick19;
            CASE 5:
                rtPickLoc:=rtPick20;
            CASE 6:
                rtPickLoc:=rtPick21;
            CASE 7:
                rtPickLoc:=rtPick22;
            CASE 8:
                rtPickLoc:=rtPick23;
            CASE 9:
                rtPickLoc:=rtPick24;
            CASE 10:
                rtPickLoc:=rtPick25;
            CASE 11:
                rtPickLoc:=rtPick26;
            CASE 12:
                rtPickLoc:=rtPick27;
            CASE 13:
                rtPickLoc:=rtPick28;
            CASE 14:
                rtPickLoc:=rtPick29;
            CASE 15:
                rtPickLoc:=rtPick30;
            DEFAULT:
            ENDTEST
            Xoffs:=0;
            Yoffs:=0;
            Wobj_Table_Temp:=Wobj_Table2;
            
            
            
            Pick Xoffs, Yoffs, rtPickLoc;
            ClearPath;
            Place;
            ClearPathReturn;
            Incr MaskPickLoc;
        ENDWHILE
        SetDO do_Starved,1;
        TableEmptyStart;
        SetDO do_Starved,0;
        ENDWHILE

    ENDPROC
    PROC Pick(num offX, num offy, robtarget PickTgt)
!        IF MaskPickLoc=1 OR MaskPickLoc=2 OR MaskPickLoc=6 THEN
!            MoveL Offs(PickTgt,offx,offy+300,Zoffs),v10k,z50,tVac\WObj:=Wobj_Table;
!        ENDIF
        WaitDI di_FanucMaskTableLoaded,0;
        MoveL Offs(PickTgt,offx,offy,Zoffs),v10k,z50,tVac\WObj:=Wobj_Table_Temp;
        !The following is for startup testing only 
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!REMOVE WHEN TESTING IS DONE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !WaitTime 10;
        TriggL Offs(PickTgt,offx,offy,0),v500,VacON,fine,tVac\WObj:=Wobj_Table_Temp;
        WaitTime nPickDwell;
        MoveL Offs(PickTgt,offx,offy,Zoffs),v500,z50,tVac\WObj:=Wobj_Table_Temp;
!        IF MaskPickLoc=1 OR MaskPickLoc=2 OR MaskPickLoc=6 THEN
!            MoveL Offs(PickTgt,offx,offy+300,Zoffs),v10k,z100,tVac\WObj:=Wobj_Table;
!        ENDIF
    ENDPROC
    
    PROC Place()
        !MoveL Offs(rtPlace,0,100,ZPlace),v10k,z100,tVac\WObj:=Wobj_Zero;
        AccSet 10,10 \FinePointRamp:=50;
        MoveL Offs(rtPlace_Cam,0,0,-ZPlace),v500,z50,tVac\WObj:=wobj_plate;
        
        !Select Camera Mode
        Camera_Selection;
        
        SetDO do_Blocked,1;
        WaitUntil bLoadMaskReady=FALSE;
        WaitDI diMaskPlaceAvailable,1; 
        SetDO do_Blocked,0;

        !If Camera Selected then Pictures, If not , Just regular Placing (Else)   
        Place_Positioning;
        !TriggL rtPlace,v500,VacOFF,fine,tVac\WObj:=Wobj_Zero;
        SetDO do_MaskPlaced,1;
        WaitTime nPlaceDwell;
        !MoveL Offs(rtPlace_Cam,ai_XOffset,ai_YOffset,-ZPlace),v500,z50,tVac\WObj:=wobj_plate;
        MoveLSync Offs(rtPlace,0,0,ZPlace),v500,z50,tVac\WObj:=Wobj_Zero,"MultiDO";
    ENDPROC
    
    PROC ClearPath()
        MoveL Offs(rtClearTooling,380,0,0),v10k,z100,tVac\WObj:=Wobj_Zero;
        MoveL rtClearTooling,v10k,z100,tVac\WObj:=Wobj_Zero;
    ENDPROC
    
    PROC ClearPathReturn()
        
        MoveL rtClearTooling,v10k,z100,tVac\WObj:=Wobj_Zero;
        MoveL Offs(rtClearTooling,380,0,0),v10k,z100,tVac\WObj:=Wobj_Zero;
    ENDPROC
    
    PROC MultiDO()

        SetDO do_MaskPlaceClear,1;
        bLoadMaskReady:=TRUE;
    ENDPROC
    
    PROC InitIO()
        SetDO do_VacON,0;
        SetDO do_LoadMaskTable,0;
        SetDO do_ClearMaskTable,0;
        SetDO do_MaskPlaced,0;
        SetDO do_MaskPlaceClear,0;
        !Initializacion Camera
        Init_camera;
        
    ENDPROC
    

FUNC dnum fFloatToDNum(num nlInput1)
        VAR num nGInum:=0;
        VAR rawbytes raw_data_IO;
        VAR dnum nlOutput1;
        ClearRawBytes raw_data_IO;
        PackRawBytes nlInput1,raw_data_IO,1\Float4;
        UnpackRawBytes raw_data_IO,1,nlOutput1\IntX:=UDINT;
        RETURN nlOutput1;
ENDFUNC
    !CAMERA ROUTINES   
    PROC Init_Camera()
        !Initialize Enabling
        CameraEnabled:=false;
    ENDPROC

    PROC Camera_Selection()
        !Camera Job Name
        CONST string myjob := "PositioningJob_JigMask";
        !When Camera Positioning HMI Selected - Load Camera Job and Running Mode
        IF (DInput(di_EnableCamera)=1  AND (CameraEnabled=false)) THEN 
            CamSetProgramMode Camera_1;
            CamLoadJob Camera_1, myjob;
            CamSetRunMode Camera_1;
            CameraEnabled := true;
        ENDIF
        !When Camera Posioning Disabled
        IF (DInput(di_EnableCamera)=0) THEN
            CameraEnabled := FALSE;
        ENDIF
    
    ENDPROC

    PROC Place_Positioning()
            
            !Selection of Camera Positioning System
            IF CameraEnabled = TRUE THEN
                !Move in Z to the Offset Position to Take Picture
                MoveL Offs(rtPlace_Cam,0,0,-20),v500,fine,tVac\WObj:=wobj_plate;
                !Req For Image
                CamReqImage Camera_1;
                Waittime 1;
                !Req For Offset Results
                CamGetParameter Camera_1, "XOffset_Mask" \numvar:=offset_X_Mask;
                CamGetParameter Camera_1, "YOffset_Mask" \numvar:=offset_y_Mask;
                CamGetParameter Camera_1, "XOffset_Jig" \numvar:=offset_X_Jig;
                CamGetParameter Camera_1, "YOffset_Jig" \numvar:=offset_y_Jig;  
                !Values send to PLC
                !First Limit Values
                IF offset_X_Mask >= 32 THEN offset_X_Mask:=32;ELSE IF offset_X_Mask <=-32 THEN offset_X_Mask:=-32; ENDIF ENDIF
                IF offset_y_Mask >= 32 THEN offset_y_Mask:=32;ELSE IF offset_y_Mask <=-32 THEN offset_y_Mask:=-32; ENDIF ENDIF
                IF offset_X_Jig >= 32 THEN offset_X_Jig:=32;ELSE IF offset_X_Jig <=-32 THEN offset_X_Jig:=-32; ENDIF ENDIF
                IF offset_y_Jig >= 32 THEN offset_y_Jig:=32;ELSE IF offset_y_Jig <=-32 THEN offset_y_Jig:=-32; ENDIF ENDIF
                
                !Send Values
                SetAo goCamera_Xoffset_Mask,Trunc(offset_X_Mask*1000);
                SetAo goCamera_Yoffset_Mask,Trunc(offset_Y_Mask*1000);
                SetAo goCamera_Xoffset_Jig,Trunc(offset_X_Jig*1000);
                SetAo goCamera_Yoffset_Jig,Trunc(offset_Y_Jig*1000);

                !goCamera_Xoffset_Mask:=offset_X_Mask;
                !goCamera_Yoffset_Mask:=offset_Y_Mask;
                !goCamera_Xoffset_Jig:=offset_X_Jig;
                !goCamera_Yoffset_Jig:=offset_Y_Jig;          
                !Sum Offsets
                offset_X_total:= offset_X_Mask + offset_X_Jig;
                offset_Y_total:= offset_Y_Mask + offset_Y_Jig;
                !Check if Offsets makes sense , if we are out of limits then ignore it
                IF offset_X_total< 2.0 AND offset_X_total>-2.0 THEN
                    offset_X:=offset_X_total;
                    offset_y:=0;
                ELSE
                    offset_X:=0;
                    offset_y:=0; 
                ENDIF
                !Write 0 until we are Sure numbers are real.
                offset_X:=0;
                offset_y:=0;      
    
                !Place Position Corrected
                TriggL Offs (rtPlace_Cam,offset_X,offset_y,0),v500,VacOFF,fine,tVac\WObj:=wobj_plate;
                
                !Move in Z to the Offset Position to Take Last Picture
                MoveL Offs(rtPlace_Cam,0,0,-50),v500,fine,tVac\WObj:=wobj_plate;
                
                 !Req For Image
                CamReqImage Camera_1;
                Waittime 1;
                !Req For Offset Results
                CamGetParameter Camera_1, "X_FinalValue" \numvar:=XFinal_Pos;
                CamGetParameter Camera_1, "Y_FinalValue" \numvar:=YFinal_Pos;
                !Send Values to PLC
                !First Limit Values
                IF XFinal_Pos >= 320 THEN XFinal_Pos:=320;ELSE IF XFinal_Pos <=-320 THEN XFinal_Pos:=-320; ENDIF ENDIF
                IF YFinal_Pos >= 320 THEN YFinal_Pos:=320;ELSE IF YFinal_Pos <=-320 THEN YFinal_Pos:=-320; ENDIF ENDIF
                !Send Values
                SetAo goCamera_XFinalPos,Trunc(XFinal_Pos*100);
                SETaO goCamera_YFinalPos,Trunc(YFinal_Pos*100);

                
    
            ELSE
                !Place 
                TriggL rtPlace,v500,VacOFF,fine,tVac\WObj:=Wobj_Zero;
            ENDIF
            
        
        
        
        
    ENDPROC




    PROC InitTriggs()
        TriggIO VacON, VacOnTime\Time\DOp:=do_VacON,1;
        TriggIO VacOFF, VacOffTime\Time\DOp:=do_VacON,0;
    ENDPROC
    
    PROC StartUp()
        VAR bool bBadEntry:=TRUE;
        !Acceleration Setting (First Acc Value, Second Ramp Value, percentages)
        AccSet 10,10 \FinePointRamp:=50;
        rtTemp:=CRobT(\Tool:=tVac\WObj:=Wobj_Zero);
        rtTemp.trans.z:=-1330;
        rtTemp1:=rtTemp;
        rtTemp1.trans.x:=-245;
        rtTemp1.trans.y:=0;
        IF rtTemp.trans.x<-550 THEN 
            rtTemp1.trans.x:=rtTemp.trans.x;
            rtTemp1.trans.y:=rtTemp.trans.y;
        ENDIF
        MoveL rtTemp,v500,z50,tVac\WObj:=Wobj_Zero;
        MoveL rtTemp1,v500,z50,tVac\WObj:=Wobj_Zero;
        MoveL Offs(rtClearTooling,300,0,0),v1000,z100,tVac\WObj:=Wobj_Zero;
        MoveL rtClearTooling,v1000,fine,tVac\WObj:=Wobj_Zero;
        SetDO do_VacON,0;
        WaitTime 0.5;
        WHILE bBadEntry DO
        !TPErase;
        !TPShow 2;
        Val:=giTableMode;
        !TPReadFK  Val,"Is the Table Empty,Full or Partly Full","EMPTY","","PARTIAL","","FULL";
        IF Val =1 THEN
            TableEmptyStart;
            bBadEntry:=FALSE;
        ELSEIF Val = 3 THEN
            TablePartialStart;
            bBadEntry:=FALSE;
        ELSEIF Val =5 THEN
            TableFullStart;
            bBadEntry:=FALSE;
        ELSE
            TPWrite "Not a valid entry Please try again";
            bBadEntry:=TRUE;
        ENDIF
        ENDWHILE
        
    ENDPROC
    
    PROC TableEmptyStart()
        MoveL rtClearTooling,v1000,fine,tVac\WObj:=Wobj_Zero;
        SetDO do_LoadMaskTable,1;
        SetDO do_ClearMaskTable,1;
        WaitUntil bTableLoaded=FALSE;
        WaitDI di_FanucMaskTableLoaded,1;
        SetDO do_LoadMaskTable,0;
        WaitUntil bTableClear=FALSE;
        WaitDI di_FanucClearMaskTable,1;
        SetDO do_ClearMaskTable,0;
        bTableLoaded:=TRUE;
        bTableClear:=TRUE;
        MaskPickLoc := 1;
        ClearPathReturn;
    ENDPROC
    
    PROC TablePartialStart()
        VAR bool bBadEntry:=TRUE;
        
        WHILE bBadEntry DO
            Val:=giPartsonTable;
            !TPReadNum Val,"How many Masks are on the table?";
            IF Val <1 OR Val > 15 THEN
                bBadEntry:=TRUE;
            ELSE
                bBadEntry:=FALSE;
            ENDIF
        ENDWHILE
        MaskPickLoc:=15-Val+1;
        ClearPathReturn;
    ENDPROC
    
    PROC TableFullStart()
        MaskPickLoc := 1;
        ClearPathReturn;
    ENDPROC
    
    PROC Start()
        PowerON;
    ENDPROC
    
    PROC PowerON()
        PulseDO do_PPMain;
        IF DInput(diRunChainOK)=0 THEN
            PulseDO do_ResetEStop;
            
        ENDIF
        WaitTime 1;
        IF DInput(diRunChainOK)=0 THEN
            TPWrite "Robot is in Estop - Make sure all safety circuits are reset!!!";
            WaitDI diRunChainOK,1;
            PulseDO do_ResetEStop;
            WaitTime 1;
        ENDIF
        PulseDO do_MotorON;
        TPWrite "Robot is ready to start ";
    ENDPROC
    
    PROC CameraCal()
        !Points to Calibrate the camera.
        MoveL rtCal_1_Point,v10k,fine,tVac\WObj:=Wobj_plate;
        MoveL rtCal_2_Point,v10k,fine,tVac\WObj:=Wobj_plate;
        MoveL rtCal_3_Point,v10k,fine,tVac\WObj:=Wobj_plate;
        MoveL rtCal_4_Point,v10k,fine,tVac\WObj:=Wobj_plate;
        MoveL rtCal_5_Point,v10k,fine,tVac\WObj:=Wobj_plate;
        
        !Validation Routine
        MoveL rtCal_1_Point,v10k,fine,tVac\WObj:=Wobj_plate;
        MoveL rtCal_Val_Point_1,v10k,fine,tVac\WObj:=Wobj_plate;
        MoveL rtCal_Val_Point_2,v10k,fine,tVac\WObj:=Wobj_plate;
        
    ENDPROC
    
    PROC Path_10()
        !MoveL rtPick1,v1000,z100,tVac\WObj:=Wobj_Table;
        !MoveL rtPick16,v1000,z100,tVac\WObj:=Wobj_Table;
        !MoveL rtPick31,v1000,z100,tVac\WObj:=Wobj_Table;
        !Table 1
        MoveL rtPick1,v1000,z100,tVac\WObj:=Wobj_Table1;
        !Table 2
        MoveL rtPick16,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick17,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick18,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick19,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick20,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick21,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick22,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick23,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick24,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick25,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick26,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick27,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick28,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick29,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick30,v1000,z100,tVac\WObj:=Wobj_Table2;
        !Table 3
        MoveL rtPick31,v1000,z100,tVac\WObj:=Wobj_Table3;
        MoveL rtPlace, v1000, z100, tVac\WObj:=Wobj_Zero;
        MoveL rtPlace_Cam, v1000, z100, tVac\WObj:=wobj_plate;
        MoveL rtClearTooling,v1000,z100,tVac\WObj:=Wobj_Zero;
    ENDPROC
ENDMODULE