MODULE RobotErrors
    !
    ! Trap and send ALL robot errors over to PLC.
    !
    VAR intnum err_int1;
    VAR errdomain err_domain;
    VAR num err_number;
    VAR errtype err_type;
    VAR trapdata err_data;
    PERS num err_code{50}:=[20148,20148,20148,10052,10138,20148,20148,20148,10052,10138,20148,20148,20148,20148,10052,10138,20148,20148,10052,10138,20148,20148,20148,20148,10230,10138,20148,20293,20148,20148,20148,20148,20148,10052,10138,20148,20148,10052,20148,10138,20148,20148,20148,20148,10052,10138,20148,20148,20148,20148];
    PERS num err_inc:=25;
    !
    PROC main()
        !
        !log all errors. send at most 1 every 150ms.
        CONNECT err_int1 WITH TrapErrs;
        IError COMMON_ERR,TYPE_ALL,err_int1;
        !
        WHILE TRUE DO
            WaitTime 1;
        ENDWHILE
        !
    ENDPROC

    TRAP TrapErrs
        ISleep err_int1;
        Incr err_inc;
        GetTrapData err_data;
        ReadErrData err_data,err_domain,err_number,err_type;
        err_code{err_inc}:=err_domain*10000+err_number;
        SetGO goErrDomain,err_domain;
        SetGO goErrNumber,err_number;
        !!use this if domain and error number are separate for xml lookup
!        SetGO goErrCode,err_code;
        ! Use this if entire error code number is desired
        Set doRERRDataReady;
        WaitTime 0.075;
        Reset doRERRDataReady;
        WaitTime 0.075;
        IWatch err_int1;
        IF err_inc>49 err_inc:=0;
    ENDTRAP

ENDMODULE