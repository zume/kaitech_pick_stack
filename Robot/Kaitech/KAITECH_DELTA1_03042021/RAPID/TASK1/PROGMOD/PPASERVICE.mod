MODULE PPASERVICE
    !***********************************************************
    !
    ! Module: PPASERVICE
    !
    ! Description
    !   This program module includes all service modules
    !   that i able to run from PickMaster.
    !   All service routine names and service variable should 
    !   be inserted in the persistant variables to be
    !   recognized by PickMaster.
    !
    !***********************************************************

    TASK PERS string ServiceRoutine1:="Home";
    TASK PERS string ServiceRoutine2:="WashDown";
    TASK PERS string ServiceRoutine3:="TestCycle";
    TASK PERS string ServiceRoutine4:="Homepos";
    TASK PERS string ServiceRoutine5:="";
    TASK PERS string ServiceRoutine6:="";
    TASK PERS string ServiceRoutine7:="";
    TASK PERS string ServiceRoutine8:="";
    TASK PERS string ServiceRoutine9:="";
    TASK PERS string ServiceRoutine10:="";

    TASK PERS string ServiceVar1:="ServVar1";
    TASK PERS string ServiceVar2:="ServVar2";
    TASK PERS string ServiceVar3:="ServVar3";
    TASK PERS string ServiceVar4:="";
    TASK PERS string ServiceVar5:="";
    TASK PERS string ServiceVar6:="";
    TASK PERS string ServiceVar7:="";
    TASK PERS string ServiceVar8:="";
    TASK PERS string ServiceVar9:="";
    TASK PERS string ServiceVar10:="";

    TASK PERS num ServVar1:=0;
    TASK PERS num ServVar2:=0;
    TASK PERS num ServVar3:=0;

    !***********************************************************
    !
    ! Procedure Home
    !
    !   Service routine
    !
    !***********************************************************
    PROC Home()
        MoveL SafePos,VeryLowSpeed,fine,Gripper\WObj:=wobj0;
        TPWrite "Home Routine executed";
        ExitCycle;
    ENDPROC

    !***********************************************************
    !
    ! Procedure WashDown
    !
    !   Service routine
    !
    !***********************************************************
    PROC WashDown()
        TPWrite "WashDown Routine executed";
        ExitCycle;
    ENDPROC

    !***********************************************************
    !
    ! Procedure TestCycle
    !
    !   Service routine
    !
    !***********************************************************
    PROC TestCycle()
        TPWrite "TestCycle Routine executed";
        ExitCycle;
    ENDPROC

    !***********************************************************
    !
    ! Procedure Homepos
    !
    !   Service routine
    !
    !***********************************************************
    PROC Homepos()
        MoveAbsJ [[-11,-11,-11,0,0,0],[0,0,0,0,0,0]]\NoEOffs,VeryLowSpeed,fine,Gripper\WObj:=wobj0;
        ExitCycle;
    ENDPROC

ENDMODULE
