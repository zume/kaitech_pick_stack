MODULE PPAMAIN
    !***********************************************************
    !
    ! File: PMppa360_IRC5.prg
    !
    ! $Revision: \main\pm_3.00_iter1\8 $
    !
    ! Description
    !   This is the main program for a pick and place 
    !   application.
    !
    ! Copyright (c) ABB Automation Technology Products 2002.
    ! All rights reserved
    !
    !***********************************************************

    !***********************************************************
    !
    ! Module: PPAMAIN
    !
    ! Description
    !   This is the main program module for a pick and 
    !   place application. 
    !
    !***********************************************************

    TASK PERS bool RestartOK:=TRUE;

    !***********************************************************
    !
    ! Procedure main
    !
    !   This is the PickMaster MAIN routine.
    !
    !***********************************************************
    PROC main()
        RestartActive:=FALSE;
        InitSafeStop;
        InitSpeed;
		CornerPathWarning FALSE;
        WaitForExeOrder;
        %RoutineName %;
    ERROR (PPA_RESTART)
        IF (ERRNO=PPA_RESTART) THEN
            RestartActive:=TRUE;
            RestoPath;
            ClearPath;
            StorePath;
            GotoRestartPos;
            RestoPath;
            StartMove;
            restartActive:=FALSE;
            RETRY;
        ENDIF
    ENDPROC

    !***********************************************************
    !
    ! Procedure InitSafeStop
    !
    !   This routine initiates the robot stop interrupt.
    !
    !***********************************************************
    PROC InitSafeStop()
        ForceStopProcess:=FALSE;
        IDelete SafeStopInt;
        CONNECT SafeStopInt WITH SafeStopTrap;
        IPers ForceStopProcess,SafeStopInt;
        ! Initiate the cartesian safe position
        SafePos:=CalcRobT(SafePosJoints,Gripper);
    ENDPROC

    !***********************************************************
    !
    ! Procedure InitTriggs
    !
    !   This routine sets the triggdata for the vacuum signals
    !   for all used item sources.
    !
    !***********************************************************
    PROC InitTriggs()
        FOR i FROM 1 TO MaxNoSources DO
            IF (ItmSrcData{i}.Used) THEN
                SetTriggs i;
            ENDIF
        ENDFOR
    ENDPROC

    !***********************************************************
    !
    ! Procedure InitPickTune
    !
    !   This routine initiates the tuning interrupt.
    !
    !***********************************************************
    PROC InitPickTune()
        PickTune:=FALSE;
        IDelete PickTuneInt;
        CONNECT PickTuneInt WITH PickTuneTrap;
        IPers PickTune,PickTuneInt;
    ENDPROC

    !***********************************************************
    !
    ! Procedure SetSimulatedDummyTriggs
    !
    !   Set up all trigg events used in the RAPID code that not is
    !   relevant for simulated mode.
    !   No need to change if the tool has 1 - 4 activators.
    !
    !***********************************************************
    PROC SetSimulatedDummyTriggs(num Index)
        TriggEquip ItmSrcData{Index}.VacuumAct1,0,ItmSrcData{Index}.VacActDelay\GOp:=goDummySignal,0;
        TriggEquip ItmSrcData{Index}.VacuumRev1,0,ItmSrcData{Index}.VacRevDelay\GOp:=goDummySignal,0;
        TriggEquip ItmSrcData{Index}.VacuumOff1,0,ItmSrcData{Index}.VacOffDelay\GOp:=goDummySignal,0;
        TriggEquip ItmSrcData{Index}.VacuumAct2,0,ItmSrcData{Index}.VacActDelay\GOp:=goDummySignal,0;
        TriggEquip ItmSrcData{Index}.VacuumRev2,0,ItmSrcData{Index}.VacRevDelay\GOp:=goDummySignal,0;
        TriggEquip ItmSrcData{Index}.VacuumOff2,0,ItmSrcData{Index}.VacOffDelay\GOp:=goDummySignal,0;
        TriggEquip ItmSrcData{Index}.VacuumAct3,0,ItmSrcData{Index}.VacActDelay\GOp:=goDummySignal,0;
        TriggEquip ItmSrcData{Index}.VacuumRev3,0,ItmSrcData{Index}.VacRevDelay\GOp:=goDummySignal,0;
        TriggEquip ItmSrcData{Index}.VacuumOff3,0,ItmSrcData{Index}.VacOffDelay\GOp:=goDummySignal,0;
        TriggEquip ItmSrcData{Index}.VacuumAct4,0,ItmSrcData{Index}.VacActDelay\GOp:=goDummySignal,0;
        TriggEquip ItmSrcData{Index}.VacuumRev4,0,ItmSrcData{Index}.VacRevDelay\GOp:=goDummySignal,0;
        TriggEquip ItmSrcData{Index}.VacuumOff4,0,ItmSrcData{Index}.VacOffDelay\GOp:=goDummySignal,0;
    ENDPROC

    !***********************************************************
    !
    ! Procedure SetSimulatedTriggs
    !
    !   Offline version of SetTriggs.
    !   No need to change if the tool has 1 - 2 activators.
    !
    !***********************************************************
    PROC SetSimulatedTriggs(num Index)
        ! 
        ! Setup all tool events for simulated mode:
        ! SimAttachX: Attach a nearby item to tool activator X.
        ! SimDetachX: Drop the item held by tool activator X.
        !
        TEST ItmSrcData{Index}.SourceType
        CASE PICK_TYPE:
            TriggEquip ItmSrcData{Index}.SimAttach1,0,0\GOp:=goVacBlow1,1;
            TriggEquip ItmSrcData{Index}.SimAttach2,0,0\GOp:=goVacBlow2,1;
            !      TriggEquip ItmSrcData{Index}.SimAttach3,0,0\GOp:=goVacBlow3,1;
            !      TriggEquip ItmSrcData{Index}.SimAttach4,0,0\GOp:=goVacBlow4,1;
        CASE PLACE_TYPE:
            TriggEquip ItmSrcData{Index}.SimDetach1,0,0\GOp:=goVacBlow1,2;
            TriggEquip ItmSrcData{Index}.SimDetach2,0,0\GOp:=goVacBlow2,2;
            !      TriggEquip ItmSrcData{Index}.SimDetach3,0,0\GOp:=goVacBlow3,2;
            !      TriggEquip ItmSrcData{Index}.SimDetach4,0,0\GOp:=goVacBlow4,2;
        ENDTEST
        SetSimulatedDummyTriggs(Index);
    ENDPROC

    !***********************************************************
    !
    ! Procedure SetDummyTriggs
    !
    !   Set up all trigg events used in the RAPID code that not is
    !   relevant for online mode.
    !   No need to change if the tool has 1 - 4 activators.
    !
    !***********************************************************
    PROC SetDummyTriggs(num Index)
        TriggEquip ItmSrcData{Index}.SimAttach1,0,0\GOp:=goDummySignal,0;
        TriggEquip ItmSrcData{Index}.SimDetach1,0,0\GOp:=goDummySignal,0;
        TriggEquip ItmSrcData{Index}.SimAttach2,0,0\GOp:=goDummySignal,0;
        TriggEquip ItmSrcData{Index}.SimDetach2,0,0\GOp:=goDummySignal,0;
        TriggEquip ItmSrcData{Index}.SimAttach3,0,0\GOp:=goDummySignal,0;
        TriggEquip ItmSrcData{Index}.SimDetach3,0,0\GOp:=goDummySignal,0;
        TriggEquip ItmSrcData{Index}.SimAttach4,0,0\GOp:=goDummySignal,0;
        TriggEquip ItmSrcData{Index}.SimDetach4,0,0\GOp:=goDummySignal,0;
    ENDPROC

    !***********************************************************
    !
    ! Procedure SetTriggs
    !
    !   This routine sets the triggdata (tool events) for the gripper.
    !   Need to match the I/O interface of the gripper control.
    !
    !***********************************************************
    PROC SetTriggs(num Index)
        IF RobOS() THEN
            ! 
            ! Setup all tool events for online mode:
            ! VacuumActX: Turn on the vacuum for tool activator X.
            ! VacuumRevX: Reverse the vacuum (blow) for tool activator X.
            ! VacuumOffX: Turn off the vacuum for tool activator X.
            !
            TEST ItmSrcData{Index}.SourceType
            CASE PICK_TYPE:
                TriggEquip ItmSrcData{Index}.VacuumAct1,0,ItmSrcData{Index}.VacActDelay\GOp:=goVacBlow1,1;
                TriggEquip ItmSrcData{Index}.VacuumAct2,0,ItmSrcData{Index}.VacActDelay\GOp:=goVacBlow2,1;
            CASE PLACE_TYPE:
                TriggEquip ItmSrcData{Index}.VacuumRev1,0,ItmSrcData{Index}.VacRevDelay\GOp:=goVacBlow1,2;
                TriggEquip ItmSrcData{Index}.VacuumOff1,0,ItmSrcData{Index}.VacOffDelay\GOp:=goVacBlow1,0;
                TriggEquip ItmSrcData{Index}.VacuumRev2,0,ItmSrcData{Index}.VacRevDelay\GOp:=goVacBlow2,2;
                TriggEquip ItmSrcData{Index}.VacuumOff2,0,ItmSrcData{Index}.VacOffDelay\GOp:=goVacBlow2,0;
            DEFAULT:
                TriggEquip ItmSrcData{Index}.VacuumAct1,0,ItmSrcData{Index}.VacActDelay\GOp:=goVacBlow1,1;
                TriggEquip ItmSrcData{Index}.VacuumRev1,0,ItmSrcData{Index}.VacRevDelay\GOp:=goVacBlow1,2;
                TriggEquip ItmSrcData{Index}.VacuumOff1,0,ItmSrcData{Index}.VacOffDelay\GOp:=goVacBlow1,0;
                TriggEquip ItmSrcData{Index}.VacuumAct2,0,ItmSrcData{Index}.VacActDelay\GOp:=goVacBlow2,1;
                TriggEquip ItmSrcData{Index}.VacuumRev2,0,ItmSrcData{Index}.VacRevDelay\GOp:=goVacBlow2,2;
                TriggEquip ItmSrcData{Index}.VacuumOff2,0,ItmSrcData{Index}.VacOffDelay\GOp:=goVacBlow2,0;
            ENDTEST
            SetDummyTriggs(Index);
        ELSE
            ! Simulated mode
            SetSimulatedTriggs(Index);
        ENDIF
    ENDPROC

    !***********************************************************
    !
    ! Procedure InitSpeed
    !
    !   This routine sets the speed limits. It may be changed
    !   in different projects.
    !
    !***********************************************************
    PROC InitSpeed()
        MaxSpeed.v_tcp:=Vtcp;
        LowSpeed.v_tcp:=Vtcp/3;
        VeryLowSpeed.v_tcp:=250;
        VelSet 100,10000;
    ENDPROC

    !***********************************************************
    !
    ! Procedure PickPlace
    !
    !   Initiate the final settings before starting the process
    !   and specify the pick-place cycle.
    !
    !***********************************************************
    PROC PickPlace()
        ConfL\Off;
        IF RestartOK=FALSE THEN
            SystemStopAction\Halt;
            RAISE PPA_RESTART;
        ENDIF
        MoveL SafePos,VeryLowSpeed,fine,Gripper\WObj:=wobj0;
        SetGO goVacBlow1,0;
        ResetAx4 ROB_ID;
        NotifyRunning;
        IF (FirstTime=TRUE) THEN
            WaitTime 1;
            FOR i FROM 1 TO MaxNoSources DO
                IF (ItmSrcData{i}.Used) THEN
                    WaitTime 0.2;
                    QStartItmSrc ItmSrcData{i}.ItemSource;
                ENDIF
            ENDFOR
            EnumerateWorkAreas;
            InitTriggs;
            InitPickTune;
            PickRateInit;
            FirstTime:=FALSE;
            WaitTime 0.2;
        ENDIF
        WHILE TRUE DO
            IF (StopProcess=TRUE) THEN
                StopProcess:=FALSE;
                SafeStop;
            ENDIF
            PickPlaceSeq;
            IncrPicks;
            IF (CheckAx4Rev()=FALSE) THEN
                MoveL SafePos,MaxSpeed,fine,Gripper\WObj:=wobj0;
                ResetAx4 ROB_ID;
            ENDIF
        ENDWHILE
    ERROR
        TEST ERRNO
        CASE PPA_RESTART:
            RAISE ;
        ENDTEST
    ENDPROC

    !***********************************************************
    !
    ! Procedure SafeStop
    !
    !   This routine execute a movement to the SafeStop
    !   position and resets some variables.
    !
    !***********************************************************
    PROC SafeStop()
        VAR string time;
        VAR string date;

        IDelete SafeStopInt;
        GotoRestartPos;
        ResetAx4 ROB_ID;
        StopProcess:=FALSE;
        ForceStopProcess:=FALSE;
        NotifySafeStop;
        ExitCycle;
    ENDPROC

    !***********************************************************
    !
    ! Procedure GotoRestartPos
    !
    !   This routine moves the robot to the SafePos and will
    !   also acknowledge all item sources so the execution can
    !   be restarted.
    !
    !***********************************************************
    PROC GotoRestartPos()
        VAR robtarget pCurrentPosition;
        VAR jointtarget jointpos1;

        RestartOK:=TRUE;
        pCurrentPosition:=CRobT(\Tool:=tool0\WObj:=wobj0);
        jointpos1:=CalcJointT(pCurrentPosition,tool0\WObj:=wobj0);
        IF RestartOK=TRUE THEN
            FOR i FROM 1 TO MaxNoSources DO
                IF (ItmSrcData{i}.Used) THEN
                    TriggL SafePos,VeryLowSpeed,ItmSrcData{i}.Nack,fine,Gripper\WObj:=wobj0;
                ENDIF
            ENDFOR
            SetGO goVacBlow1,0;
        ENDIF

    ERROR
        IF ERRNO=ERR_ROBLIMIT THEN
            ErrWrite "Robot is outside working range",RobName()+" must be jogged inside working range.";
            RestartOK:=FALSE;
            TRYNEXT;
        ELSE
            RAISE ;
        ENDIF
    ENDPROC

    !**********************************************************
    !
    ! Trap SafeStopTrap
    !
    !   This trap will run the SafeStop routine.
    !
    !**********************************************************
    TRAP SafeStopTrap
        IF (ForceStopProcess=TRUE) THEN
            ForceStopProcess:=FALSE;
            ClearPath;
            StorePath;
            SafeStop;
            RestoPath;
            StartMove;
        ENDIF
    ENDTRAP

    !**********************************************************
    !
    ! Trap PickTuneTrap
    !
    !   This trap sets the tune datas.
    !
    !**********************************************************
    TRAP PickTuneTrap
        IF (PickTune=TRUE) THEN
            PickTune:=FALSE;
            TEST TuneType
            CASE SPEED_TUNE:
                MaxSpeed.v_tcp:=Vtcp;
                LowSpeed.v_tcp:=Vtcp/3;
            CASE PICKPLACE_TUNE:
                IF (ItmSrcData{SourceIndex}.Used) THEN
                    IF (ItmSrcData{SourceIndex}.SourceType=PICK_TYPE) THEN
                        ItmSrcData{SourceIndex}.VacActDelay:=VacActDelay;
                    ELSEIF (ItmSrcData{SourceIndex}.SourceType=PLACE_TYPE) THEN
                        ItmSrcData{SourceIndex}.VacRevDelay:=VacRevDelay;
                        ItmSrcData{SourceIndex}.VacOffDelay:=VacOffDelay;
                    ELSE
                        ItmSrcData{SourceIndex}.VacActDelay:=VacActDelay;
                        ItmSrcData{SourceIndex}.VacRevDelay:=VacRevDelay;
                        ItmSrcData{SourceIndex}.VacOffDelay:=VacOffDelay;
                    ENDIF
                    IF (ItmSrcData{SourceIndex}.TrackPoint.Type=fllwtime) THEN
                        ItmSrcData{SourceIndex}.TrackPoint.followtime:=FollowTime;
                    ELSEIF (ItmSrcData{SourceIndex}.TrackPoint.Type=stoptime) THEN
                        ItmSrcData{SourceIndex}.TrackPoint.stoptime:=FollowTime;
                    ENDIF
                    ItmSrcData{SourceIndex}.OffsZ:=OffsZ;
                    SetTriggs SourceIndex;
                ELSE
                    ErrWrite "Tune not possible ","The workarea index "+ValToStr(SourceIndex)+" is an unused index";
                ENDIF
            ENDTEST
        ENDIF
    ENDTRAP

ENDMODULE
