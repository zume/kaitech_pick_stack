#
# Generated backup file, do not edit.
#
# Originally at: /hd0a/TEMP/backup.215163
# 2021-03-04  18:51:03
#
>>SYSTEM_ID:
360-800936

>>PRODUCTS_ID:
RobotWare Version: 6.10.02.00

  RobICI I/O Driver
  RobotWare Base
  English
  709-1 DeviceNet Master/Slave
  841-1 EtherNet/IP Scanner/Adapter
  1552-1 Tracking Unit Interface
  608-1 World Zones
  610-1 Independent Axis
  613-1 Collision Detection
  616-1 PC Interface
  642-1 PickMaster 3
  Three conveyor work areas
  One I/O board DeviceNet
  Motor Commutation
  Service Info System
  Pendelum Calibration
  IRB 360-6/1600 Standard
  Drive System IRB 120/140/260/360/910SC/1200/1400/1520/1600/1660ID IRC5 Compact

>>HOME:
All public files and directories

>>SYSPAR:
SYS.cfg
PROC.cfg
MMC.cfg
EIO.cfg
SIO.cfg
MOC.cfg


>>TASK1: (T_ROB1,RapE425,)
PROGMOD/PPAMAIN.mod @ 
SYSMOD/user.sys @ 
PROGMOD/PPAEXECUTING.mod @ 
PROGMOD/PPASERVICE.mod @ 
SYSMOD/ppaUser.sys @ 
SYSMOD/PPAUTILITIES.sys @ 


>>EOF:
