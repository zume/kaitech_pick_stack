MODULE PPAEXECUTING
    !***********************************************************
    !
    ! Module: PPAEXECUTING
    !
    ! Description
    !   This program module executes all movements when the
    !   process is running.
    !   Edit this module to customize the project.
    !
    !***********************************************************

    VAR num PickWorkArea{MaxNoSources};
    VAR num PlaceWorkArea{MaxNoSources};
    VAR num OtherWorkArea{MaxNoSources};

    ! Definition of the item targets, are temporarely used in the pick and place routines.
    VAR itmtgt PickTarget;
    VAR itmtgt PlaceTarget;
    TASK PERS wobjdata WObjPick:=[FALSE,FALSE,"CNV2",[[0,0,0],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];
    TASK PERS wobjdata WObjPlace:=[FALSE,FALSE,"CNV3",[[0,0,0],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];

    ! Safe/stop position. Given as joint angles.
    TASK PERS jointtarget SafePosJoints:=[[-11,-11,-11,0,0,0],[0,0,0,0,0,0]];
    ! Cartesian safe/stop position. Calculated from SafePosJoints and Gripper.
    TASK PERS robtarget SafePos:=[[-7.58506E-06,0,-1263.11],[0,1,0,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];

    ! Set the payload for the items which is picked.
    TASK PERS loaddata ItemLoad:=[0.001,[0,0,0.001],[1,0,0,0],0,0,0];


    !***********************************************************
    !
    ! Procedure EnumerateWorkAreas
    !
    !   Enumerate all work areas.
    !
    !   PickWorkArea{1} becomes the pick work area with the lowest selection index.
    !   PickWorkArea{2} becomes the pick work area with the second lowest selection index.
    !   etc.
    !
    !   PlaceWorkArea{1} becomes the place work area with the lowest selection index.
    !   PlaceWorkArea{2} is the place work area with the second lowest selection index.
    !   etc.
    !
    !   OtherWorkArea{1} becomes the place work area with the lowest selection index.
    !   OtherWorkArea{2} becomes the place work area with the second lowest selection index.
    !   etc.
    !
    !***********************************************************
    PROC EnumerateWorkAreas()
        VAR num PickNumber:=1;
        VAR num PlaceNumber:=1;
        VAR num OtherNumber:=1;

        FOR i FROM 1 TO MaxNoSources DO
            IF (ItmSrcData{i}.Used) THEN
                IF (ItmSrcData{i}.SourceType=PICK_TYPE) THEN
                    PickWorkArea{PickNumber}:=i;
                    Incr PickNumber;
                ELSEIF (ItmSrcData{i}.SourceType=PLACE_TYPE) THEN
                    PlaceWorkArea{PlaceNumber}:=i;
                    Incr PlaceNumber;
                ELSE
                    OtherWorkArea{OtherNumber}:=i;
                    Incr OtherNumber;
                ENDIF
            ENDIF
        ENDFOR
    ENDPROC

    !***********************************************************
    !
    ! Procedure PickPlaceSeq
    !
    !   The Pick and Place sequence. 
    !   Edit this routine to modify from which work areas to pick and place. 
    !   Needs to be changed if more than one pick work area is used.
    !   Needs to be changed if more than one place work area is used.
    !
    !***********************************************************
    PROC PickPlaceSeq()
        Pick PickWorkArea{1};
        !    Pick PickWorkArea{2};
        Place PlaceWorkArea{1};
        !    Place PlaceWorkArea{2};
    ENDPROC

    !***********************************************************
    !
    ! Procedure Pick
    !
    !   Executes a pick movement.
    !   Edit this routine to modify how the robot shall 
    !   execute the pick movements.
    !   Needs to be changed if more than one activator is used.
    !
    !***********************************************************
    PROC Pick(num Index)
        IF Index>0 THEN
            WObjPick:=ItmSrcData{Index}.Wobj;
            GetItmTgt ItmSrcData{Index}.ItemSource,PickTarget;
            SetToolRot PickTarget,PickAct1,Index;
            TriggL\Conc,RelTool(PickTarget.RobTgt,0,0,-ItmSrcData{Index}.OffsZ),MaxSpeed,ItmSrcData{Index}.VacuumAct1,z20,PickAct1\WObj:=WObjPick;
            TriggL\Conc,PickTarget.RobTgt,LowSpeed,ItmSrcData{Index}.SimAttach1,z5\Inpos:=ItmSrcData{Index}.TrackPoint,PickAct1\WObj:=WObjPick;
            GripLoad ItemLoad;
            TriggL RelTool(PickTarget.RobTgt,0,0,-ItmSrcData{Index}.OffsZ),LowSpeed,ItmSrcData{Index}.Ack,z20,PickAct1\WObj:=WObjPick;
        ELSE
            ErrWrite "Missing item distribution","Cannot pick because no item distribution contains current work area."\RL2:="Please check configuration";
            SafeStop;
        ENDIF
    ENDPROC

    !***********************************************************
    !
    ! Procedure Place
    !
    !   Executes a place movement.
    !   Edit this routine to modify how the robot shall 
    !   execute the place movements.
    !   Needs to be changed if more than one activator is used.
    !
    !***********************************************************
    PROC Place(num Index)
        IF Index>0 THEN
            WObjPlace:=ItmSrcData{Index}.Wobj;
            GetItmTgt ItmSrcData{Index}.ItemSource,PlaceTarget;
            MoveL\Conc,RelTool(PlaceTarget.RobTgt,0,0,-ItmSrcData{Index}.OffsZ),MaxSpeed,z20,PickAct1\WObj:=WObjPlace;
            TriggL\Conc,PlaceTarget.RobTgt,LowSpeed,ItmSrcData{Index}.VacuumRev1\T2:=ItmSrcData{Index}.VacuumOff1\T3:=ItmSrcData{Index}.SimDetach1,z5\Inpos:=ItmSrcData{Index}.TrackPoint,PickAct1\WObj:=WObjPlace;
            GripLoad load0;
            TriggL RelTool(PlaceTarget.RobTgt,0,0,-ItmSrcData{Index}.OffsZ),LowSpeed,ItmSrcData{Index}.Ack,z20,PickAct1\WObj:=WObjPlace;
        ELSE
            ErrWrite "Missing item distribution","Cannot place because no item distribution contains current work area."\RL2:="Please check configuration";
            SafeStop;
        ENDIF
    ENDPROC

ENDMODULE
