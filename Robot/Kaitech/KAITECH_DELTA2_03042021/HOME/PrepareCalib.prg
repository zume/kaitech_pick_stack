MODULE PREPARECALIB
  !***********************************************************
  !
  ! Module: PREPARECALIB
  !
  ! Description
  !   Purpose of program module:
  !
  !   1) Prepare conveyor work areas
  !   CNV1, CNV2, etc for calibration.
  !
  !   2) Prepare indexed work areas for work object definition
  !
  ! Copyright (c) ABB Robotics Products AB 2018.  
  ! All rights reserved
  !
  !***********************************************************
  TASK PERS wobjdata WorkObject:=[FALSE,TRUE,"",[[0,0,0],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];
  VAR mecunit CnvMecUnit;
  VAR string CnvMecString:="";
  VAR datapos block;
  VAR num CnvNum:=0;
  VAR num IndNum:=0;
  VAR num CalibType:=0;
  VAR num ConfirmSave:=0;
  VAR intnum NewObj;
  VAR bool NewObjReported:=FALSE;
  VAR signaldo doCntToEncStr;
  VAR signaldo doNewObjStrobe;
  VAR signaldo doRemAllPObj;
  VAR signaldo doDropWObj;
  VAR signaldo doPosInJobQ;
  VAR signaldo doSoftSync;
  VAR signalao aoPosition;

  !========================== main ===========================
  PROC main()
    TPErase;
    CalibType:=0;
    WorkObject:=wobj0;
    CnvName:="";
    NonCnvWobjName:="";
    TPWrite "Prepare a work area for calibration.";
    TPReadFK CalibType,"Select work area type","","","","Conveyor","Stationary";
    IF CnvName<>"" OR CalibType=4 THEN
      TPReadFK CnvNum,"Choose conveyor work area","CNV1","CNV2","CNV3","CNV4","MORE";
      IF CnvNum=5 THEN
        TPReadFK CnvNum,"Choose conveyor work area","CNV5","CNV6","","","";
        CnvNum:=CnvNum+4;
      ENDIF
      CnvName:="CNV"+ValToStr(CnvNum);
      GetDataVal "c"+ValToStr(CnvNum)+"RemAllPObj",doRemAllPObj;
      GetDataVal "c"+ValToStr(CnvNum)+"DropWObj",doDropWObj;
      GetDataVal "c"+ValToStr(CnvNum)+"PosInJobQ",doPosInJobQ;
      GetDataVal "c"+ValToStr(CnvNum)+"SoftSync",doSoftSync;
      GetDataVal "c"+ValToStr(CnvNum)+"CntToEncStr",doCntToEncStr;
      GetDataVal "c"+ValToStr(CnvNum)+"NewObjStrobe",doNewObjStrobe;
      GetDataVal "c"+ValToStr(CnvNum)+"Position",aoPosition;
      SetDataSearch "mecunit"\Object:=CnvName;
      IF GetNextSym(CnvMecString,block) THEN
        GetDataVal CnvMecString\Block:=block,CnvMecUnit;
      ELSE
        ErrWrite "Selected conveyor work area is not installed",CnvName;
        Stop;
      ENDIF
      WorkObject.ufmec:=CnvName;
      WorkObject.ufprog:=FALSE;
      TPWrite "Deleting objects from CNV"+ValToStr(CnvNum);
      NewObjReported:=FALSE;
      IDelete NewObj;
      SetDO doNewObjStrobe,0;
      SetDO doCntToEncStr,0;
      SetDO doRemAllPObj,0;
      SetDO doDropWObj,0;
      WaitTime 0.5;
      PulseDO doRemAllPObj;
      WaitTime 0.5;
      PulseDO doDropWObj;
      WaitTime 0.5;
      SetDO doPosInJobQ,1;
      ActUnit CnvMecUnit;
      TPWrite "Activating CNV"+ValToStr(CnvNum);
      WaitTime 0.5;
      ppaDropWObj WorkObject;
      WaitTime 0.5;
      !
      CONNECT NewObj WITH ObjTrap;
      ISignalDO doCntToEncStr,1,NewObj;
      PulseDO doNewObjStrobe;
      WaitUntil NewObjReported=TRUE;
      SetSysData Gripper;
      SetSysData WorkObject;
      TPWrite "CNV"+ValToStr(CnvNum)+" is prepared for calibration";
      Stop;
    ELSEIF NonCnvWobjName<>"" OR CalibType=5 THEN
      TPReadFK IndNum,"Choose WorkObject","IdxWobj1","IdxWobj2","IdxWobj3","IdxWobj4","MORE";
      IF IndNum=5 THEN
        TPReadFK IndNum,"Choose WorkObject","IdxWobj5","IdxWobj6","IdxWobj7","IdxWobj8","MORE";
        IndNum:=IndNum+4;
        IF IndNum=9 THEN
          TPReadFK IndNum,"Choose WorkObject","IdxWobj9","IdxWobj10","IdxWobj11","IdxWobj12","MORE";
          IndNum:=IndNum+8;
          IF IndNum=13 THEN
            TPReadFK IndNum,"Choose WorkObject","IdxWobj13","IdxWobj14","IdxWobj15","IdxWobj16","MORE";
            IndNum:=IndNum+12;
            IF IndNum=17 THEN
              TPReadFK IndNum,"Choose WorkObject","IdxWobj17","IdxWobj18","IdxWobj19","IdxWobj20","MORE";
              IndNum:=IndNum+16;
              IF IndNum=21 THEN
                TPReadFK IndNum,"Choose WorkObject","IdxWobj21","IdxWobj22","IdxWobj23","IdxWobj24","IdxWobj25";
                IndNum:=IndNum+20;
              ENDIF
            ENDIF
          ENDIF
        ENDIF
      ENDIF
      NonCnvWobjName:="IdxWobj"+ValToStr(IndNum);
      SetSysData Gripper;
      SetSysData WorkObject;
      TPWrite "Define current work object";
      TPWrite " ";
      TPWrite "Continue rapid execution when ready";
      TPWrite "to save the work object definition";
      WaitTime 2;
      Stop;
      TPWrite " ";
      TPReadFK ConfirmSave,"Do you want to save this work object definition ?","","","","Yes","No";
      IF ConfirmSave=4 THEN
        FOR i FROM 1 TO MaxNoSources DO
          IF NonCnvWOData{i}.NonCnvWobjName=NonCnvWobjName THEN
            NonCnvWOData{i}.Used:=TRUE;
            NonCnvWOData{i}.Wobj:=WorkObject;
          ENDIF
        ENDFOR
        Save "ppaUser"\FilePath:=diskhome\File:="ppaUser.sys";
      ENDIF
    ENDIF
  ENDPROC

  !======================== ObjTrap =========================
   TRAP ObjTrap
    TPWrite "Resetting position for CNV"+ValToStr(CnvNum);
    TPWrite "Position = "\Num:=AOutput(aoPosition) ;
    WaitTime 0.2;
    NewObjReported:=TRUE;
    RETURN;
  ENDTRAP
ENDMODULE
