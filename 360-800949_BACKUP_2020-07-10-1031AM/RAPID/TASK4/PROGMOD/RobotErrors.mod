MODULE RobotErrors
    !
    ! Trap and send ALL robot errors over to PLC.
    !
    VAR intnum err_int1;
    VAR errdomain err_domain;
    VAR num err_number;
    VAR errtype err_type;
    VAR trapdata err_data;
    PERS num err_code{50}:=[10012,10010,10205,10205,71482,10010,10230,50024,50024,50024,50024,50024,50024,50024,50024,50024,50024,50024,50024,50024,50024,50024,10140,50024,50024,50024,50024,50024,50024,50024,50024,50024,50024,50024,50024,50024,10125,10079,10230,10230,10233,10230,10205,71405,10205,10205,90216,10012,10010,90216];
    PERS num err_inc:=7;
    !
    PROC main()
        !
        !log all errors. send at most 1 every 150ms.
        CONNECT err_int1 WITH TrapErrs;
        IError COMMON_ERR,TYPE_ALL,err_int1;
        !
        WHILE TRUE DO
            WaitTime 1;
        ENDWHILE
        !
    ENDPROC

    TRAP TrapErrs
        ISleep err_int1;
        Incr err_inc;
        GetTrapData err_data;
        ReadErrData err_data,err_domain,err_number,err_type;
        err_code{err_inc}:=err_domain*10000+err_number;
        SetGO goErrDomain,err_domain;
        SetGO goErrNumber,err_number;
        !!use this if domain and error number are separate for xml lookup
!        SetGO goErrCode,err_code;
        ! Use this if entire error code number is desired
        Set doRERRDataReady;
        WaitTime 0.075;
        Reset doRERRDataReady;
        WaitTime 0.075;
        IWatch err_int1;
        IF err_inc>49 err_inc:=0;
    ENDTRAP

ENDMODULE