MODULE ReportsModule
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !
    !  This module generates system status for reporting to MES
    !  cycle time is generated and the last 45 times are saved in the array LastCycleTime
    !  Paused or running is set in Run_Paused where 1 is running and 2 is paused
    !  Picking or placing is set in bPicking and bPlacing. Each is set TRUE when doing that task
    !  There are two virtual outputs that are set for the conditions Starved and Blocked. They are do_Starved and do_Blocked
    !  Speed % is set in speedOvrd. It will be a number from 0 to 100
    !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    VAR clock CycleTimeclk;
    PERS num CurrentcycleTime;
    PERS num LastCycleTime{45}:=[11.225,10.862,10.214,10.913,10.874,10.217,11,10.89,10.89,10.213,10.999,10.92,10.719,11.1,11.29,15.637,10.84,15.233,11.286,28.612,11.021,11.072,11.839,10.007,10.016,10.088,10.363,10.64,10.367,10.633,10.306,9.927,9.056,9.088,9.068,9.052,9.064,9.544,9.074,9.077,9.108,5.169,5.836,1043.22,2177.78];
    VAR bool bCurrentState:=FALSE;
    VAR intnum IntVacOff;
    VAR intnum IntVacOn;
    PERS num SpeedOvrd:=50;
    PERS num Auto_Man:=1;
    !Auto = 1 Manual = 2
    PERS num Run_Paused:=2;
    !running = 1 Paused = 2
    PERS bool bPlacing:=FALSE;
    PERS bool bPicking:=TRUE;

    PROC main()
        InitInts;
        WHILE TRUE DO
            ClkStart CycleTimeclk;
            SpeedOvrd:=CSpeedOverride();
            IF DOutput(do_InAuto)=1 THEN
                Auto_Man:=1;
            ELSE
                Auto_Man:=2;
            ENDIF
            IF DOutput(do_TaskExec)=1 THEN
                Run_Paused:=1;
            ELSE
                Run_Paused:=2;
            ENDIF
            WaitTime 0.05;
        ENDWHILE

    ENDPROC

    PROC InitInts()
        CONNECT IntVacOff WITH TimeTrap;
        ISignalDO do_VacON,0,IntVacOff;
        CONNECT IntVacOn WITH PlaceTrap;
        ISignalDO do_VacON,1,IntVacOn;
    ENDPROC

    TRAP TimeTrap
        bPicking:=TRUE;
        bPlacing:=FALSE;
        FOR i FROM 45 TO 2 STEP -1 DO
            LastCycleTime{i}:=LastCycleTime{i-1};
        ENDFOR
        ClkStop CycleTimeclk;
        LastCycleTime{1}:=CurrentcycleTime;
        CurrentcycleTime:=ClkRead(CycleTimeclk);
        ClkReset CycleTimeclk;
        ClkStart CycleTimeclk;
    ENDTRAP

    TRAP PlaceTrap
        bPicking:=FALSE;
        bPlacing:=TRUE;
    ENDTRAP
ENDMODULE