MODULE mDataModule
	PERS tooldata tVac:=[TRUE,[[0,0,305],[1,0,0,0]],[0.3,[0,0,75],[1,0,0,0],0,0,0]];
    
!	TASK PERS wobjdata Wobj_Table:=[FALSE,TRUE,"",[[494.31,-641.459,-1694.17],[0.861809,-0.00479171,0.00162969,0.507207]],[[0,0,0],[1,0,0,0]]];
    TASK PERS wobjdata Wobj_Zero:=[FALSE,TRUE,"",[[0,0,0],[0.862029,-0.00506621,0.00168196,0.506831]],[[0,0,0],[1,0,0,0]]];
    
!    TASK PERS wobjdata Wobj_Table:=[FALSE,TRUE,"",[[493.921,-641.946,-1694.38],[0.861978,-0.00458628,0.00160928,0.506922]],[[0,0,0],[1,0,0,0]]];
    TASK PERS wobjdata Wobj_Table:=[FALSE,TRUE,"",[[456.757,-526.054,-1613.28],[0.861201,-0.00316998,0.000532212,0.508255]],[[0,0,0],[1,0,0,0]]];
    TASK PERS wobjdata Wobj_Table1 := [FALSE, TRUE, "", [[450.084, -538.657, -1613.51],[0.861344, -0.00875607, -0.00334216, 0.507936]],[[0, 0, 0],[1, 0, 0, 0]]];
    TASK PERS wobjdata Wobj_Table2 := [FALSE, TRUE, "", [[21.3307, -295.228, -1620.67],[0.861911, -0.00126587, -0.00469588, 0.507036]],[[0, 0, 0],[1, 0, 0, 0]]];
    TASK PERS wobjdata Wobj_Table3 := [FALSE, TRUE, "", [[-418.633, -46.3274, -1619.93],[0.864343, 0.00375817, 0.00147358, 0.502887]],[[0, 0, 0],[1, 0, 0, 0]]];
    PERS wobjdata Wobj_Table_Temp := [FALSE, TRUE, "", [[450.084, -538.657, -1613.51],[0.861344, -0.00875607, -0.00334216, 0.507936]],[[0, 0, 0],[1, 0, 0, 0]]];
    
	CONST robtarget rtPick1:=[[-2.04,8,0.44],[0.00751336,-0.690256,0.723505,0.00560252],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPick16:=[[-2.12,8,0.68],[6.19237E-05,0.691086,-0.722756,-0.00486312],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPick31:=[[-2.06,8,0.63],[0.00321421,0.694539,-0.719444,0.00244217],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtPlace:=[[-747.08,22.54,-1494.26],[0.00533293,-0.690952,0.722881,-0.000235239],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtClearTooling:=[[-615.00,343.96,-1422.72],[0.00533323,-0.690025,0.723766,-0.0002284],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget rtTemp;
    PERS robtarget rtTemp1;
    PERS robtarget rtPickLoc;
    CONST robtarget rtClearPath1:=[[-577.48,343.96,-1455.20],[0.00533323,-0.690025,0.723766,-0.0002284],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtClearPath2:=[[-577.48,343.96,-1455.20],[0.00533323,-0.690025,0.723766,-0.0002284],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtClearPath3:=[[-577.48,343.96,-1455.20],[0.00533323,-0.690025,0.723766,-0.0002284],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rtClearPath4:=[[-577.48,343.96,-1455.20],[0.00533323,-0.690025,0.723766,-0.0002284],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    

    VAR triggdata VacON;
    VAR triggdata VacOFF;
    
    PERS speeddata v10k:=[10000,5000,1,1];
    
    PERS bool bFirstTime:=TRUE;
    PERS bool bLoadMaskReady:=FALSE;
    PERS bool bTableLoaded:=FALSE;
    PERS bool bTableClear:=FALSE;
    
    VAR num Xoffs:=0;
    VAR num Yoffs:=0;
    VAR num Zoffs:=100;
    VAR num ZPlace:=65;
    VAR num j;
    VAR num VacOnTime:=0.05;!use 0 for simulation use 0.05 to ?? for real application
    VAR num VacOffTime:=0.07;!use 0 for simulation use 0.05 to ?? for real application
    VAR num Val;
    PERS num MaskPickLoc:=1;
    VAR num nPickDwell:=0.1;
    VAR num nPlaceDwell:=0.25;
    PERS num nXoffSet:=176.5;
	TASK PERS tooldata tPointer:=[TRUE,[[0,0,300],[1,0,0,0]],[0.3,[0,0,75],[1,0,0,0],0,0,0]];
    
    
    
    
ENDMODULE