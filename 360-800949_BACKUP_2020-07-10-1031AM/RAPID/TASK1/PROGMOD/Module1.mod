MODULE Module1
        
    !***********************************************************
    !
    ! Module:  Module1
    !
    ! Description:
    !   <Insert description here>
    !
    ! Author: steve
    !
    ! Version: 1.0
    !
    !***********************************************************
    
    
    !***********************************************************
    !
    ! Procedure main
    !
    !   This is the entry point of your program
    !
    !***********************************************************
    PROC main()
        InitTriggs;
        InitIO;
        StartUp;
        CornerPathWarning FALSE;
        WHILE TRUE DO
        WHILE MaskPickLoc <= 45 DO

            TEST MaskPickLoc
            CASE 1,2,3,4,5:
                Yoffs:=0;
                Xoffs:=(MaskPickLoc-1)*nXoffSet;
                rtPickLoc:=rtPick1;
                Wobj_Table_Temp:=Wobj_Table1;
            CASE 6,7,8,9,10:
                Yoffs:=143;
                Xoffs:=(MaskPickLoc-6)*nXoffSet;
                rtPickLoc:=rtPick1;
                Wobj_Table_Temp:=Wobj_Table1;
            CASE 11,12,13,14,15:
                Yoffs:=286;
                Xoffs:=(MaskPickLoc-11)*nXoffSet;
                rtPickLoc:=rtPick1;
                Wobj_Table_Temp:=Wobj_Table1;
            CASE 16,17,18,19,20:
!                Yoffs:=490;
                Yoffs:=0;
                Xoffs:=(MaskPickLoc-16)*nXoffSet;
                rtPickLoc:=rtPick16;
                Wobj_Table_Temp:=Wobj_Table2;
            CASE 21,22,23,24,25:
!                Yoffs:=490+143;
                Yoffs:=143;
                Xoffs:=(MaskPickLoc-21)*nXoffSet;
                rtPickLoc:=rtPick16;
                Wobj_Table_Temp:=Wobj_Table2;
            CASE 26,27,28,29,30:
!                Yoffs:=490+286;
                Yoffs:=286;
                Xoffs:=(MaskPickLoc-26)*nXoffSet;
                rtPickLoc:=rtPick16;
                Wobj_Table_Temp:=Wobj_Table2;
            CASE 31,32,33,34,35:
!                Yoffs:=980;
                Yoffs:=0;
                Xoffs:=(MaskPickLoc-31)*nXoffSet;
                rtPickLoc:=rtPick31;
                Wobj_Table_Temp:=Wobj_Table3;
            CASE 36,37,38,39,40:
!                Yoffs:=980+143;
                Yoffs:=143;
                Xoffs:=(MaskPickLoc-36)*nXoffSet;
                rtPickLoc:=rtPick31;
                Wobj_Table_Temp:=Wobj_Table3;
            CASE 41,42,43,44,45:
 !               Yoffs:=980+286;
                Yoffs:=286;
                Xoffs:=(MaskPickLoc-41)*nXoffSet;
                rtPickLoc:=rtPick31;
                Wobj_Table_Temp:=Wobj_Table3;
            DEFAULT:
            ENDTEST
            Pick Xoffs, Yoffs, rtPickLoc;
            ClearPath;
            Place;
            ClearPathReturn;
            Incr MaskPickLoc;
        ENDWHILE
        SetDO do_Starved,1;
        TableEmptyStart;
        SetDO do_Starved,0;
        ENDWHILE

    ENDPROC
    PROC Pick(num offX, num offy, robtarget PickTgt)
!        IF MaskPickLoc=1 OR MaskPickLoc=2 OR MaskPickLoc=6 THEN
!            MoveL Offs(PickTgt,offx,offy+300,Zoffs),v10k,z50,tVac\WObj:=Wobj_Table;
!        ENDIF
        MoveL Offs(PickTgt,offx,offy,Zoffs),v10k,z50,tVac\WObj:=Wobj_Table_Temp;
        TriggL Offs(PickTgt,offx,offy,0),v500,VacON,fine,tVac\WObj:=Wobj_Table_Temp;
        WaitTime nPickDwell;
        MoveL Offs(PickTgt,offx,offy,Zoffs),v500,z50,tVac\WObj:=Wobj_Table_Temp;
!        IF MaskPickLoc=1 OR MaskPickLoc=2 OR MaskPickLoc=6 THEN
!            MoveL Offs(PickTgt,offx,offy+300,Zoffs),v10k,z100,tVac\WObj:=Wobj_Table;
!        ENDIF
    ENDPROC
    
    PROC Place()
        MoveL Offs(rtPlace,0,100,ZPlace),v10k,z100,tVac\WObj:=Wobj_Zero;
        MoveL Offs(rtPlace,0,0,ZPlace),v500,z50,tVac\WObj:=Wobj_Zero;
        SetDO do_Blocked,1;
        WaitUntil bLoadMaskReady=FALSE;
        WaitDI diMaskPlaceAvailable,1; 
        SetDO do_Blocked,0;
        TriggL rtPlace,v500,VacOFF,fine,tVac\WObj:=Wobj_Zero;
        SetDO do_MaskPlaced,1;
        WaitTime nPlaceDwell;
        MoveLSync Offs(rtPlace,0,0,ZPlace),v500,z50,tVac\WObj:=Wobj_Zero,"MultiDO";
        MoveL Offs(rtPlace,0,100,ZPlace),v10k,z100,tVac\WObj:=Wobj_Zero;
    ENDPROC
    
    PROC ClearPath()
        MoveL Offs(rtClearTooling,350,0,-50),v10k,z100,tVac\WObj:=Wobj_Zero;
        MoveL rtClearTooling,v10k,z100,tVac\WObj:=Wobj_Zero;
    ENDPROC
    
    PROC ClearPathReturn()
        
        MoveL rtClearTooling,v10k,z100,tVac\WObj:=Wobj_Zero;
        MoveL Offs(rtClearTooling,350,0,0),v10k,z100,tVac\WObj:=Wobj_Zero;
    ENDPROC
    
    PROC MultiDO()

        SetDO do_MaskPlaceClear,1;
        bLoadMaskReady:=TRUE;
    ENDPROC
    
    PROC InitIO()
        SetDO do_VacON,0;
        SetDO do_LoadMaskTable,0;
        SetDO do_ClearMaskTable,0;
        SetDO do_MaskPlaced,0;
        SetDO do_MaskPlaceClear,0;
    ENDPROC
    
    PROC InitTriggs()
        TriggIO VacON, VacOnTime\Time\DOp:=do_VacON,1;
        TriggIO VacOFF, VacOffTime\Time\DOp:=do_VacON,0;
    ENDPROC
    
    PROC StartUp()
        VAR bool bBadEntry:=TRUE;
        rtTemp:=CRobT(\Tool:=tVac\WObj:=Wobj_Zero);
        rtTemp.trans.z:=-1450;
        rtTemp1:=rtTemp;
        rtTemp1.trans.x:=-300;
        rtTemp1.trans.y:=350;
        IF rtTemp.trans.x<-550 THEN 
            rtTemp1.trans.x:=rtTemp.trans.x;
            rtTemp1.trans.y:=200;
        ENDIF
        MoveL rtTemp,v500,z50,tVac\WObj:=Wobj_Zero;
        MoveL rtTemp1,v500,z50,tVac\WObj:=Wobj_Zero;
        MoveL Offs(rtClearTooling,300,0,0),v1000,z100,tVac\WObj:=Wobj_Zero;
        MoveL rtClearTooling,v1000,fine,tVac\WObj:=Wobj_Zero;
        SetDO do_VacON,0;
        WaitTime 0.5;
        WHILE bBadEntry DO
        TPErase;
        TPShow 2;
        TPReadFK  Val,"Is the Table Empty,Full or Partly Full","EMPTY","","PARTIAL","","FULL";
        IF Val =1 THEN
            TableEmptyStart;
            bBadEntry:=FALSE;
        ELSEIF Val = 3 THEN
            TablePartialStart;
            bBadEntry:=FALSE;
        ELSEIF Val =5 THEN
            TableFullStart;
            bBadEntry:=FALSE;
        ELSE
            TPWrite "Not a valid entry Please try again";
            bBadEntry:=TRUE;
        ENDIF
        ENDWHILE
        
    ENDPROC
    
    PROC TableEmptyStart()
        MoveL rtClearTooling,v1000,fine,tVac\WObj:=Wobj_Zero;
        SetDO do_LoadMaskTable,1;
        SetDO do_ClearMaskTable,1;
        WaitUntil bTableLoaded=FALSE;
        WaitDI di_FanucMaskTableLoaded,1;
        SetDO do_LoadMaskTable,0;
        WaitUntil bTableClear=FALSE;
        WaitDI di_FanucClearMaskTable,1;
        SetDO do_ClearMaskTable,0;
        bTableLoaded:=TRUE;
        bTableClear:=TRUE;
        MaskPickLoc := 1;
        ClearPathReturn;
    ENDPROC
    
    PROC TablePartialStart()
        VAR bool bBadEntry:=TRUE;
        
        WHILE bBadEntry DO
        TPReadNum Val,"How many Masks are on the table?";
        IF Val <1 OR Val > 45 THEN
            bBadEntry:=TRUE;
        ELSE
            bBadEntry:=FALSE;
        ENDIF
        ENDWHILE
        MaskPickLoc:=45-Val+1;
        ClearPathReturn;
    ENDPROC
    
    PROC TableFullStart()
        MaskPickLoc := 1;
        ClearPathReturn;
    ENDPROC
    
    PROC Start()
        PowerON;
    ENDPROC
    
    PROC PowerON()
        PulseDO do_PPMain;
        IF DInput(diRunChainOK)=0 THEN
            PulseDO do_ResetEStop;
            
        ENDIF
        WaitTime 1;
        IF DInput(diRunChainOK)=0 THEN
            TPWrite "Robot is in Estop - Make sure all safety circuits are reset!!!";
            WaitDI diRunChainOK,1;
            PulseDO do_ResetEStop;
            WaitTime 1;
        ENDIF
        PulseDO do_MotorON;
        TPWrite "Robot is ready to start ";
    ENDPROC
    
    PROC Path_10()
        !MoveL rtPick1,v1000,z100,tVac\WObj:=Wobj_Table;
        !MoveL rtPick16,v1000,z100,tVac\WObj:=Wobj_Table;
        !MoveL rtPick31,v1000,z100,tVac\WObj:=Wobj_Table;
        MoveL rtPick1,v1000,z100,tVac\WObj:=Wobj_Table1;
        MoveL rtPick16,v1000,z100,tVac\WObj:=Wobj_Table2;
        MoveL rtPick31,v1000,z100,tVac\WObj:=Wobj_Table3;
        MoveL rtPlace,v1000,z100,tVac\WObj:=Wobj_Zero;
        MoveL rtClearTooling,v1000,z100,tVac\WObj:=Wobj_Zero;
    ENDPROC
ENDMODULE